﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {
	
	public float speed, tilt;
	public Boundary boundary;
	public float fireRate;
	private float nextFire = 0.0f;
	
	public GameObject shot;
	public Transform shotSpawn;
	
	// Update is called once per frame
	void Update(){
		if(Input.GetButton("Fire1") && Time.time > nextFire){
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
		}
		
	}
	
	// FixedUpdate is called once per physics step
	void FixedUpdate () {
		
		//Receive input and change velocity
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody>().velocity = movement * speed;
		
		//Sets boundaries to prevent the ship from leaving the play area
		GetComponent<Rigidbody>().position = new Vector3(
			Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
		);
		
		//Controls tilting when in motion
		GetComponent<Rigidbody>().rotation = Quaternion.Euler(
			0.0f, 
			0.0f, 
			GetComponent<Rigidbody>().velocity.x * -tilt
		);
	}
}
