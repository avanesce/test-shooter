﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnHit : MonoBehaviour {

	public GameObject explosion;

	//Destroys itself and the other object when hit
	void OnTriggerEnter(Collider other){
		if (other.tag == "Boundary" || other.tag == "Enemy") {
			return;
		}
		Instantiate(explosion, transform.position, transform.rotation);
		Object.DestroyObject(other.gameObject);
		Object.DestroyObject(gameObject);
	}




}
