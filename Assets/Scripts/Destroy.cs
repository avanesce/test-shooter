﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

	// Destroy any object to leave the area
	void OnTriggerExit(Collider other){
		Destroy(other.gameObject);
	}
}
