﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoundaryAlien {
	public float xMin, xMax, zMin, zMax;
}

public class RandomMove : MonoBehaviour {
	public float speed, tilt;
	public BoundaryAlien boundary;

	void Start() {
		float moveHorizontal = speed;
		float rand = Random.Range(0.0f, 1.0f);
		rand = Mathf.Round (rand);
		if (rand == 0.0f)
			rand = -1.0f;
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, 1.0f);
		GetComponent<Rigidbody>().velocity = Vector3.Scale(new Vector3(rand, 1, 1), movement) * speed;
	}

	void FixedUpdate() {
		//Receive input and change velocity
		float moveHorizontal = speed;

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, 1.0f);

		if (GetComponent<Rigidbody>().position.x < -10) 
			GetComponent<Rigidbody>().velocity = movement * speed;
		if (GetComponent<Rigidbody>().position.x > 10) 
			GetComponent<Rigidbody>().velocity = Vector3.Scale (new Vector3 (-1, 1, 1), movement) * speed;
		
		//Sets boundaries to prevent the ship from leaving the play area
		GetComponent<Rigidbody>().position = new Vector3(
			Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
		);
		
		//Controls tilting when in motion
		GetComponent<Rigidbody>().rotation = Quaternion.Euler(
			0.0f, 
			0.0f, 
			GetComponent<Rigidbody>().velocity.x * -tilt
		);
	}
}
