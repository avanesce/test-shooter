﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour {

	// Update is called once per frame
	void Update(){
		Destroy ();
	}

	void Destroy(){
		Object.Destroy (gameObject, 3.0f);
	}
}
